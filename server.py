import random
import socket
import time

mySocket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
mySocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
mySocket.bind(('', 43234))

print("Server started")

while True:
    print("\n")

    message, address = mySocket.recvfrom(1024)

    print(f"Accepted connection from {address}\n")
    print(f"Message recieved: {message.decode()}\n")

    if random.randint(0, 9) > 1:
        print("Sending response\n")
        mySocket.sendto(message.upper(), address)
    else:
        print("Emulating package loss\n")
    
    print(f"Connection from {address} closed\n")
