import datetime
import time
import socket

mySocket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
mySocket.settimeout(1.0)

lost = 0
totalRTT = 0
maxRRT = 0
minRTT = 1000000

for pings in range(1, 11):
    print("\n")
    request = f"Ping {pings} {datetime.datetime.now().time()}".encode()
    reqOut = datetime.datetime.now()
    mySocket.sendto(request, ("127.0.0.1", 43234))
    try:
        response, _ = mySocket.recvfrom(1024)
        localRTT = (datetime.datetime.now() - reqOut).microseconds

        totalRTT += localRTT
        maxRRT = max(maxRRT, localRTT)
        minRTT = min(minRTT, localRTT)

        print(f"Recived response: {response.decode()}\n")
        print(f"Current RTT stats:\nMin RTT: {minRTT} microseconds\nMax RTT: {maxRRT} microseconds\nAverage RTT: {totalRTT / pings} microseconds\n")
    except socket.timeout:
        print("It seems, that package has been lost\n")
        lost += 1

if (lost == 1):
    print(f"\nFrom 10 packeges {lost} was lost. This way loss percentage is {lost * 10}%\n")
else:
    print(f"\nFrom 10 packeges {lost} were lost. This way loss percentage is {lost * 10}%\n")
